/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.yanin.Databaseproject;
/**
 *
 * @author USER
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class ConnectDatabase {
    

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:DBdcoffee.db";
        try {
            // create a connection to the database
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        } finally{
            if(conn!=null){
                try{
                    conn.close();
                }catch (SQLException ex){
                    System.out.println(ex.getMessage());
                }
            }
        } 

    }
}
